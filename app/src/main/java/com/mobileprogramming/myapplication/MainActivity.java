package com.mobileprogramming.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button clickMeBtn = (Button) findViewById(R.id.clickMeBtn);
        OnClickListener onClickListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText txt = (EditText) findViewById(R.id.textInput);
                TextView label = (TextView) findViewById(R.id.label);
                String name = txt.getText().toString();
                if (name == "" || name.trim() == "") {
                    Toast.makeText(MainActivity.this, "Nama tidak boleh kosong", Toast.LENGTH_SHORT).show();
                } else {
                    label.setText(name);
                }
            }
        };
        clickMeBtn.setOnClickListener(onClickListener);
    }
}